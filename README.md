# Songbook

Generates a nicely formatted leadsheet given a plain-text representation of
lyrics and chords.

## References

https://tex.stackexchange.com/questions/147839/chords-lyrics-songbook
