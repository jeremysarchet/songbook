#! /bin/bash

source_dir='songs';
dest_dir='build';
output_filename='songbook.tex';

rm -rf $dest_dir; mkdir $dest_dir

echo "Generating LaTeX source...";

if perl latexify.pl \
  $source_dir \
  $dest_dir/$output_filename \
  'Jeremy Sarchet' \
; then
  echo "Done generating LaTeX source";
else
  echo "Failed to generate LaTeX source";
  exit 1;
fi;


echo "Compiling LaTeX, generating a PDF...";

cd $dest_dir;

# Got to compile at least twice to get table of contents
# https://tex.stackexchange.com/a/1606
pdflatex $output_filename
pdflatex $output_filename
