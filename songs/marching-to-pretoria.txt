title: Marching to Pretoria
author: The Highwaymen
album: March on, Brothers
date: 1972
source: https://www.traditionalmusic.co.uk/folk-songs-with-chords/Marching%20To%20Pretoria.htm
---

C
Sing with me, I'll sing with you

And so we will sing together
G7
So we will sing together
C
So we will sing together

Sing with me, I'll sing with you

And so we will sing together
G7           C
As we march along

[Chorus]

           F              C
Oh, we are marching to Pretoria
   G7        C
Pretoria, Pretoria
           F              C
Oh, we are marching to Pretoria
   G7        C
Pretoria, hooray!

Dance with me, I'll dance with you
And so we will dance together
So we will dance together
So we will dance together
Dance with me, I'll dance with you
And so we will dance together
As we march along

[Chorus]

March with me, I'll march with you
And so we will march together
So we will march together
So we will march together
March with me, I'll march with you
And so we will march together
As we march along

[Chorus]

You drink with me, I'll drink with you
And so we will drink together
So we will drink together
So we will drink together
Drink with me, I'll drink with you
And so we will drink together
As we march along

[Chorus, 2x]
