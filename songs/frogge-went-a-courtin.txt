title: Froggie Went A-Courtin'
author: Bob Dylan
album: Good As I Been To You
date: 1992
source: https://www.youtube.com/watch?v=dfgWVueAuhY
------

G - - - | G - - - | G - - - | D - - - |

G - G7 - | C - C7 - | G - D - | G - - - |

G7: 320001 (index on 1 on hi-E)
C : drop ring finger to 6th for bass note
C7: like c but with pinky on G

G(7): E-strum, D-strum
D: D-strum, OpenA-strum
C(7): A-strum, E(3rd fret)-strum


Frog went a-courtin',
and he did ride, Uh-huh,
Frog went a-courtin',
and he did ride, Uh-huh,
Frog went a-courtin',
and he did ride.
With a sword and a pistol
by his side, Uh-huh.

Well he rode up to Miss Mousey's door,
Gave three loud raps and a very big roar,

Said, "Miss Mouse, are you within?"
"Yes, kind sir, I sit and spin"

He took Miss Mousey on his knee,
Said, "Miss Mousey, will you marry me?"

"Without my uncle Rat's consent,
I wouldn't marry the president,

Uncle Rat laughed
and he shook his fat sides,
To think his niece would be a bride,

Uncle Rat went runnin' downtown,
To buy his niece a wedding gown,

Where shall the wedding supper be?
Way down yonder in a hollow tree,

What should the wedding supper be?
Fried mosquito in a black-eye pea,

Well, first to come in was a flyin' moth,
She laid out the table cloth,

Next to come in was a juney bug,
She brought the water jug,

Next to come in was a bumbley bee,
Sat mosquito on his knee,

Next to come in was a broken black flea,
Danced a jig with the bumbley bee,

Next to come in was Mrs. Cow,
She tried to dance
but she didn't know how,

Next to come in was a little black tick,
She ate so much she made us sick,

Next to come in was a big black snake,
Ate up all of the wedding cake,

Next to come was the old gray cat,
Swallowed the mouse and ate up the rat,

Mr. Frog went a-hoppin' up
over the brook,
A lily-white duck come and
swallowed him up,

A little piece of cornbread
layin' on a shelf,
If you want anymore,
you can sing it yourself,
