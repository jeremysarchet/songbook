title: Gent normal
author: Manel
date: 2009
source: https://www.cifraclub.com.br/manel/la-gent-normal/
---

C G C G
F C G

S'havia estat cultivant per Grècia i havia après que és tan important viatjar
I jo, jo me l'escoltava i deia hm, sí sí, sí sí, clar, clar"

Son pare acumulava grans riqueses, i vaig dir: "car-am!
En aquest cas si us plau, anul·li, la cervesa i posi'ns, posi'ns el vi car".
Li va semblar genial. Va fer un glopet, em va mirar, i va dir:

"Vull viure com viuen els altres,
Vull fer les coses que fa la gent normal
Vull dormir amb qui dormen els altres.
T’estic parlant de ficar-me al llit amb gent normal com tu"
I assumint, assumint aquell paper, vaig dir: "Bé, veurem què s'hi pot fer"

Vaig passejar-la pel mercat del barri
Em va semblar un escenari adequat, per començar
Vaig dir: "d'acord. Ara fes veure que no tens ni un duro"
I va riure i va dir: "ai quina gràcia, que boig estàs, ets mooolt divertit"
Doncs, francament, maca, no em sembla que ningú estigui rient aquí
Ja t'ho has pensat bé això de

Viure com ho fan els altres.  Veure les coses que veu la gent normal
Dormir amb qui dormen els altres.
Ficar-te al llit amb gent normal com jo
Però ella no entenia res, i m'agafava del bracet

Comparteix pis amb estranys, busca una feina formal
Puja al metro pels matins, ves al cine alguna nit
Però igualment mai entendràs el què és anar passant els anys
Esperant la solució, que s'emporti tanta por

No tu mai viuràs com viuen els altres.
Ni patiràs com pateix la gent normal
Mai entendràs el fracàs dels altres,
mai comprendràs com els somnis
se'ns van quedant en riure i beure,
i anar tirant, i si pots, ja saps,
follar de tant en tant

Prova a cantar si ho fan els altres,
i canta fort si et sembla interessant
riu a pulmó si ho fan els altres,
però no t'estranyi, si et gires,
que riguin de tu
Que no et sorprengui si estan farts
de tu jugant a ser com és la gent normal

C
Vull dormir amb gent normal com tu (x4)
