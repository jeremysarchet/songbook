title: Let Me Go
author: Cake
album: Prolonging the Magic
date: 1998
source: https://tabs.ultimate-guitar.com/tab/cake/let-me-go-chords-3020168
------

Note: Try substituting a B7 for the B/A

[Intro]
A D A E
A D A E
 

[Verse]
A                     A
  When she walks, she swings her arms
D               E
 Instead of her hips
A                    A
 When she talks, she moves her mouth
D               E
 Instead of her lips
 

[Bridge]
         A                    D
And I've waited for her for so long
     A                    E
I've waited for her for so long
     A                      D
I've wondered if I could hang on
  A                   E       E
I wonder if I could hang on,     yeah
 

[Chorus]
A                   B/A
Let me go she said, let me go she said
D             E
Let me go and I will want you more
A          B/A
 Let me go  Let me go
D              E   E      E7     E7
 Let me go and I - will - want - you
 

[Solo]
A B/A D E
A D A E
 

[Verse]
 When she wants, she wants the sun
 Instead of the moon
 When she sees, she sees the stars
 Inside of her room
 

[Bridge]


[Chorus]
 

[Solo]
A B/A D E
A B/A D E
 

[Chorus]
A                    B/A
 Let me go she said, let me go she said
D                   E
Let me go she said,  let me go-o
A                    B/A
 Let me go she said,  Let me go
D          E
Let me go,  let me go-o
A          B/A
Let me go,  let me go
D          E
Let me go,  let me go-o
A          B/A
Let me go,  let me go
D          E
Let me go,  let me go-o
